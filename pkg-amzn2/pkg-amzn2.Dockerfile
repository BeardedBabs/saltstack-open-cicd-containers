FROM amazonlinux:2

RUN yum install -y \
    vim-enhanced \
    gcc \
    rpm-build \
    rpm-devel \
    rpmlint \
    make \
    bash \
    coreutils \
    diffutils \
    patch \
    rpmdevtools \
    git \
    redhat-rpm-config \
    rpmdevtools \
    xz \
    libffi-devel \
    bzip2-devel \
    xz-devel \
    ncurses-devel \
    gdbm-devel \
    sqlite-devel \
    readline-devel \
    zlib-devel \
    libuuid-devel \
    ruby-devel \
    rubygems \
    wget \
    curl \
    xmlsec1-openssl \
    xmlsec1-devel \
    libtool-ltdl-devel \
    && yum groupinstall -y 'Development Tools' \
    && rm -rf /var/cache/yum/*

# http://bugs.python.org/issue19846
# > At the moment, setting "LANG=C" on a Linux system *fundamentally breaks Python 3*, and that's not OK.
ENV LANG C.UTF-8
ENV PYVER '3.9.14'
ENV PYPRE '3.9'
ENV PYSHORT '39'
ENV OPENSSLVER '1.1.1q'

RUN curl -LO https://www.openssl.org/source/openssl-${OPENSSLVER}.tar.gz \
    && tar -xvf openssl-${OPENSSLVER}.tar.gz \
    && cd openssl-${OPENSSLVER} \
    && ./config shared --prefix=/usr/local/openssl11 --openssldir=/usr/local/openssl11 \
    && make \
    && make install \
    && cd .. \
    && rm -rf openssl-${OPENSSLVER}.tar.gz openssl-${OPENSSLVER} \
    && ldconfig /usr/local/

RUN curl -LO https://www.python.org/ftp/python/${PYVER}/Python-${PYVER}.tar.xz \
    && tar xvf Python-${PYVER}.tar.xz \
    && cd Python-${PYVER} \
    && LDFLAGS="-Wl,-rpath=/usr/local/openssl11/lib" ./configure --with-openssl=/usr/local/openssl11 --with-system-ffi --enable-shared \
    && make  \
    && make install  \
    && cd ..  \
    && rm -rf Python-${PYVER}.tar.xz Python-${PYVER} \
    && ldconfig /usr/local/lib

RUN gem install --no-document 'ffi:<1.13' 'fpm:=1.11.0'
