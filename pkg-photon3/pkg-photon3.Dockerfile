FROM photon:3.0

RUN tdnf install -y \
    vim \
    awk \
    build-essential \
    rpm-build \
    rpm-devel \
    rpm-lang \
    rpm-libs \
    python3-rpm \
    python3-setuptools \
    python3-devel \
    python3-pip \
    bash \
    coreutils \
    diffutils \
    patch \
    git \
    gmp \
    gmp-devel \
    xz \
    libffi \
    libffi-devel \
    bzip2-devel \
    xz-devel \
    ncurses-devel \
    gdbm-devel \
    sqlite-devel \
    readline-devel \
    zlib-devel \
    ruby \
    wget \
    curl \
    libltdl \
    libltdl-devel \
    libxslt-devel \
    libxslt \
    linux \
    util-linux \
    util-linux-libs \
    xmlsec1 \
    xmlsec1-devel \
    && rm -rf /var/cache/tdnf/*

RUN gem install --no-document public_suffix:4.0.7 fpm
