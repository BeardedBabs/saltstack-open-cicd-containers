FROM registry.gitlab.com/saltstack/open/cicd/containers/pkg-debian10:latest

RUN groupadd signer \
    && useradd -d /home/signer -g signer -m signer

USER signer
