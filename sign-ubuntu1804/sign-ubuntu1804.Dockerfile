FROM registry.gitlab.com/saltstack/open/cicd/containers/pkg-ubuntu1804:latest

RUN groupadd signer \
    && useradd -d /home/signer -g signer -m signer

USER signer
