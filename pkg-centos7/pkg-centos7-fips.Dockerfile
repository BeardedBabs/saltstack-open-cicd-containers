FROM centos:7

RUN yum install -y \
    vim-enhanced \
    gcc \
    rpm-build \
    rpm-devel \
    rpmlint \
    make \
    bash \
    coreutils \
    diffutils \
    patch \
    rpmdevtools \
    redhat-rpm-config \
    xz \
    libffi-devel \
    bzip2-devel \
    xz-devel \
    ncurses-devel \
    gdbm-devel \
    sqlite-devel \
    readline-devel \
    zlib-devel \
    libuuid-devel \
    ruby-devel \
    rubygems \
    wget \
    curl \
    xmlsec1-openssl \
    xmlsec1-devel \
    libtool-ltdl-devel \
    https://repo.ius.io/ius-release-el7.rpm \
    && yum groupinstall -y 'Development Tools' \
    && yum remove -y git* \
    && yum install -y git236 \
    && rm -rf /var/cache/yum/*

# http://bugs.python.org/issue19846
# > At the moment, setting "LANG=C" on a Linux system *fundamentally breaks Python 3*, and that's not OK.
ENV LANG C.UTF-8
ENV PYVER '3.9.14'
ENV PYPRE '3.9'
ENV PYSHORT '39'

# Pull in Makefile and build openssl 1.0.2u and fips module
COPY pkg-centos7/Makefile /makessl/Makefile

RUN cd /makessl && make \
    && curl -LO https://www.python.org/ftp/python/${PYVER}/Python-${PYVER}.tar.xz \
    && tar xvf Python-${PYVER}.tar.xz \
    && cd Python-${PYVER} \
    && LDFLAGS="-Wl,-rpath=/usr/local/openssl102/lib" ./configure --with-openssl=/usr/local/openssl102 --with-system-ffi --enable-shared \
    && make  \
    && make install  \
    && cd ..  \
    && rm -rf Python-${PYVER}.tar.xz Python-${PYVER} \
    && ldconfig /usr/local/lib

RUN gem install --no-document 'ffi:<1.13' 'fpm:=1.11.0'
